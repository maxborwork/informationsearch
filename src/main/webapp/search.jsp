<%@ page import="java.util.List" %>
<%@ page import="by.BSUIR.documentSearch.model.Result" %><%--
  Created by IntelliJ IDEA.
  User: Alena Pashkevich
  Date: 13.09.2019
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Поисковая система ОАО "Борисевич И Пашкевич. Ты не спрячешься."</title>
    <style>

    </style>
</head>
<body>
<form action="" method="POST">
    <%
       String reqToSearch =(String) request.getAttribute("reqToSearch");
        if (reqToSearch != null && !reqToSearch.equals("")) {
            out.println("<input type=\"text\" size=\"100\" width=\"20\" style=\"display: inline-block;\" name=\"search\""
                    + "value = \""+reqToSearch+"\" placeholder=\"Введите запрос\">");
        } else {
            out.println("<input type=\"text\" size=\"100\" width=\"20\" style=\"display: inline-block;\"" +
                        " placeholder=\"Введите запрос\" name=\"search\">");
        }

    %>
    <button type="submit" value="Поиск" style="display: inline-block;"  name="searchButton" >Поиск</button>
</form>
<%
    List<Result> results = (List<Result>) request.getAttribute("resultList");
   String error = ( String) request.getAttribute("error");

    if(error.equals("true")){
        out.println("Запрос некорректен. Исправьте его и попробуйте снова.");
    } else {
        if (results != null && !results.isEmpty()) {
            out.println("<ul>");
            for (Result result : results) {
                out.println("<li>");
                out.println("<div>");
                out.println("<a href =\"/documentSearch_war/document?path="+result.getPath()+"\">"+result.getName()+"</a>");
                out.println("<br/>");
                out.print("Слова: ");
                for (String word : result.getWords()) {
                    if(result.getWords().indexOf(word)!= result.getWords().size() -1){
                        out.print(word +", ");
                    } else {
                        out.println(word +". ");
                    }
                }
                out.println("</div>");
                out.println("</li>");
                out.println("<br/>");
                out.println("<br/>");
                out.println("<br/>");
            }
            out.println("</ul>");

    }

    }
%>

</body>
</html>
