package by.BSUIR.documentSearch.servlet;

import by.BSUIR.documentSearch.controller.DocumentController;
import by.BSUIR.documentSearch.controller.FileController;
import by.BSUIR.documentSearch.controller.MainController;
import by.BSUIR.documentSearch.model.Document;
import by.BSUIR.documentSearch.model.Result;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetrixServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("metrix.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        try{
            Float a = Float.parseFloat((String) req.getParameter("a"));
            Float b = Float.parseFloat((String) req.getParameter("b"));
            Float c =Float.parseFloat((String) req.getParameter("c"));
            Float d = Float.parseFloat((String) req.getParameter("d"));
req.setAttribute("a", a);
            List<String> values = new ArrayList<String>();
            values.add(String.valueOf(a/(a+c)));
            values.add(String.valueOf(a/(a+b)));
            values.add(String.valueOf((a+d)/(a+b+c+d)));
            values.add(String.valueOf((b+c)/(a+b+c+d)));
            values.add(String.valueOf( 2.0/  ( (1.0/ (  a/  (a+b)  ) ) + (1.0/( a/ (a+c)))  )  ));
            values.add(String.valueOf(   (a/ (a+b))/5.0));
            values.add(String.valueOf(   (a/ (a+b))/10.0));

            values.add(String.valueOf(   (a/ (a+b))/(a+c)));
            req.setAttribute("values", values);
        } catch(Exception e){
            e.printStackTrace();

            req.setAttribute("error", "Проверьте введённые данные");
        }




        doGet(req, resp);
    }
}

