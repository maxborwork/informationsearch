package by.BSUIR.documentSearch.servlet;

import by.BSUIR.documentSearch.Constant;
import by.BSUIR.documentSearch.controller.FileController;
import by.BSUIR.documentSearch.controller.MainController;
import by.BSUIR.documentSearch.model.Document;
import by.BSUIR.documentSearch.model.Result;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchServlet extends HttpServlet {
    private String reqToSearch = "";
    private List<Result> resultList = new ArrayList<>();
    private Boolean firstRequest = true;
    private List<Document> documentList;
    private Boolean errorExist = false;

    private Logger log = Logger.getLogger(SearchServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        if( errorExist){
            req.setAttribute("error", "true");
        } else {
            req.setAttribute("error", "false");
        }

        req.setAttribute("resultList", resultList);
        req.setAttribute("reqToSearch", reqToSearch);
        req.getRequestDispatcher("search.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String searchRequest = req.getParameter("search");

        Pattern pattern = Pattern.compile("\\d");
        Matcher matcher = pattern.matcher(searchRequest);

        errorExist = matcher.find();

        FileController controller = new FileController();

        if (firstRequest) {
            createStopList();
            controller.parseDirectory("/home/maksim/Документы/informationsearch/documents");
            firstRequest = false;
        }

        MainController process = new MainController(searchRequest, controller);
        log.info("start getListForResponse()");
        resultList = process.getListForResponse();

        reqToSearch = searchRequest;
        doGet(req, resp);
        log.info("request done");
    }

    private void createStopList() {
        Constant.stopWords.add("а");
        Constant.stopWords.add("будем");
        Constant.stopWords.add("будет");
        Constant.stopWords.add("будешь");
        Constant.stopWords.add("будете");
        Constant.stopWords.add("буду");
        Constant.stopWords.add("будут");
        Constant.stopWords.add("будучи");
        Constant.stopWords.add("будь");
        Constant.stopWords.add("будьте");
        Constant.stopWords.add("бы");
        Constant.stopWords.add("был");
        Constant.stopWords.add("была");
        Constant.stopWords.add("были");
        Constant.stopWords.add("было");
        Constant.stopWords.add("быть");
        Constant.stopWords.add("в");
        Constant.stopWords.add("вам");
        Constant.stopWords.add("вас");
        Constant.stopWords.add("вами");
        Constant.stopWords.add("весь");
        Constant.stopWords.add("во");
        Constant.stopWords.add("вот");
        Constant.stopWords.add("все");
        Constant.stopWords.add("всё");
        Constant.stopWords.add("всего");
        Constant.stopWords.add("всей");
        Constant.stopWords.add("всем");
        Constant.stopWords.add("всём");
        Constant.stopWords.add("всеми");
        Constant.stopWords.add("всему");
        Constant.stopWords.add("всех");
        Constant.stopWords.add("всею");
        Constant.stopWords.add("всея");
        Constant.stopWords.add("всю");
        Constant.stopWords.add("вся");
        Constant.stopWords.add("вы");
        Constant.stopWords.add("да");
        Constant.stopWords.add("для");
        Constant.stopWords.add("до");
        Constant.stopWords.add("его");
        Constant.stopWords.add("едим");
        Constant.stopWords.add("едят");
        Constant.stopWords.add("ее");
        Constant.stopWords.add("её");
        Constant.stopWords.add("ей");
        Constant.stopWords.add("ел");
        Constant.stopWords.add("ела");
        Constant.stopWords.add("ем");
        Constant.stopWords.add("ему");
        Constant.stopWords.add("если");
        Constant.stopWords.add("ест");
        Constant.stopWords.add("есть");
        Constant.stopWords.add("ешь");
        Constant.stopWords.add("еще");
        Constant.stopWords.add("ещё");
        Constant.stopWords.add("ею");
        Constant.stopWords.add("же");
        Constant.stopWords.add("за");
        Constant.stopWords.add("и");
        Constant.stopWords.add("из");
        Constant.stopWords.add("или");
        Constant.stopWords.add("им");
        Constant.stopWords.add("ими");
        Constant.stopWords.add("их");
        Constant.stopWords.add("к");
        Constant.stopWords.add("как");
        Constant.stopWords.add("кем");
        Constant.stopWords.add("ко");
        Constant.stopWords.add("когда");
        Constant.stopWords.add("кому");
        Constant.stopWords.add("которая");
        Constant.stopWords.add("которого");
        Constant.stopWords.add("которое");
        Constant.stopWords.add("который");
        Constant.stopWords.add("котором");
        Constant.stopWords.add("которому");
        Constant.stopWords.add("которою");
        Constant.stopWords.add("которые");
        Constant.stopWords.add("которой");
        Constant.stopWords.add("которым");
        Constant.stopWords.add("которых");
        Constant.stopWords.add("кто");
        Constant.stopWords.add("меня");
        Constant.stopWords.add("мне");
        Constant.stopWords.add("мной");
        Constant.stopWords.add("мною");
        Constant.stopWords.add("мог");
        Constant.stopWords.add("могла");
        Constant.stopWords.add("могли");
        Constant.stopWords.add("могло");
        Constant.stopWords.add("могу");
        Constant.stopWords.add("могут");
        Constant.stopWords.add("мое");
        Constant.stopWords.add("моё");
        Constant.stopWords.add("моего");
        Constant.stopWords.add("моей");
        Constant.stopWords.add("моем");
        Constant.stopWords.add("моём");
        Constant.stopWords.add("моему");
        Constant.stopWords.add("моею");
        Constant.stopWords.add("можем");
        Constant.stopWords.add("может");
        Constant.stopWords.add("можете");
        Constant.stopWords.add("можешь");
        Constant.stopWords.add("мои");
        Constant.stopWords.add("мой");
        Constant.stopWords.add("моим");
        Constant.stopWords.add("моими");
        Constant.stopWords.add("моих");
        Constant.stopWords.add("мочь");
        Constant.stopWords.add("мою");
        Constant.stopWords.add("моя");
        Constant.stopWords.add("мы");
        Constant.stopWords.add("на");
        Constant.stopWords.add("нам");
        Constant.stopWords.add("нами");
        Constant.stopWords.add("нас");
        Constant.stopWords.add("наш");
        Constant.stopWords.add("наша");
        Constant.stopWords.add("наше");
        Constant.stopWords.add("нашей");
        Constant.stopWords.add("нашего");
        Constant.stopWords.add("нашем");
        Constant.stopWords.add("нашему");
        Constant.stopWords.add("наши");
        Constant.stopWords.add("нашим");
        Constant.stopWords.add("нашими");
        Constant.stopWords.add("наших");
        Constant.stopWords.add("нашу");
        Constant.stopWords.add("не");
        Constant.stopWords.add("него");
        Constant.stopWords.add("нее");
        Constant.stopWords.add("неё");
        Constant.stopWords.add("ней");
        Constant.stopWords.add("нем");
        Constant.stopWords.add("нём");
        Constant.stopWords.add("нему");
        Constant.stopWords.add("нет");
        Constant.stopWords.add("нею");
        Constant.stopWords.add("ним");
        Constant.stopWords.add("ними");
        Constant.stopWords.add("них");
        Constant.stopWords.add("но");
        Constant.stopWords.add("о");
        Constant.stopWords.add("об");
        Constant.stopWords.add("один");
        Constant.stopWords.add("одна");
        Constant.stopWords.add("одни");
        Constant.stopWords.add("одним");
        Constant.stopWords.add("одними");
        Constant.stopWords.add("одних");
        Constant.stopWords.add("одно");
        Constant.stopWords.add("одного");
        Constant.stopWords.add("одной");
        Constant.stopWords.add("одном");
        Constant.stopWords.add("одному");
        Constant.stopWords.add("одною");
        Constant.stopWords.add("одну");
        Constant.stopWords.add("он");
        Constant.stopWords.add("она");
        Constant.stopWords.add("они");
        Constant.stopWords.add("оно");
        Constant.stopWords.add("от");
        Constant.stopWords.add("по");
        Constant.stopWords.add("при");
        Constant.stopWords.add("с");
        Constant.stopWords.add("сам");
        Constant.stopWords.add("сама");
        Constant.stopWords.add("сами");
        Constant.stopWords.add("самим");
        Constant.stopWords.add("самими");
        Constant.stopWords.add("самих");
        Constant.stopWords.add("само");
        Constant.stopWords.add("самого");
        Constant.stopWords.add("самом");
        Constant.stopWords.add("самому");
        Constant.stopWords.add("саму");
        Constant.stopWords.add("свое");
        Constant.stopWords.add("своё");
        Constant.stopWords.add("своего");
        Constant.stopWords.add("своей");
        Constant.stopWords.add("своем");
        Constant.stopWords.add("своём");
        Constant.stopWords.add("своему");
        Constant.stopWords.add("своею");
        Constant.stopWords.add("свои");
        Constant.stopWords.add("свой");
        Constant.stopWords.add("своим");
        Constant.stopWords.add("своими");
        Constant.stopWords.add("своих");
        Constant.stopWords.add("свою");
        Constant.stopWords.add("своя");
        Constant.stopWords.add("себе");
        Constant.stopWords.add("себя");
        Constant.stopWords.add("собой");
        Constant.stopWords.add("собою");
        Constant.stopWords.add("так");
        Constant.stopWords.add("такая");
        Constant.stopWords.add("такие");
        Constant.stopWords.add("таким");
        Constant.stopWords.add("такими");
        Constant.stopWords.add("таких");
        Constant.stopWords.add("такого");
        Constant.stopWords.add("такое");
        Constant.stopWords.add("такой");
        Constant.stopWords.add("таком");
        Constant.stopWords.add("такому");
        Constant.stopWords.add("такою");
        Constant.stopWords.add("такую");
        Constant.stopWords.add("те");
        Constant.stopWords.add("тебе");
        Constant.stopWords.add("тебя");
        Constant.stopWords.add("тем");
        Constant.stopWords.add("теми");
        Constant.stopWords.add("тех");
        Constant.stopWords.add("то");
        Constant.stopWords.add("тобой");
        Constant.stopWords.add("тобою");
        Constant.stopWords.add("того");
        Constant.stopWords.add("той");
        Constant.stopWords.add("только");
        Constant.stopWords.add("том");
        Constant.stopWords.add("тому");
        Constant.stopWords.add("тот");
        Constant.stopWords.add("ту");
        Constant.stopWords.add("ты");
        Constant.stopWords.add("у");
        Constant.stopWords.add("уже");
        Constant.stopWords.add("чего");
        Constant.stopWords.add("чем");
        Constant.stopWords.add("чём");
        Constant.stopWords.add("чему");
        Constant.stopWords.add("что");
        Constant.stopWords.add("чтобы");
        Constant.stopWords.add("эта");
        Constant.stopWords.add("эти");
        Constant.stopWords.add("этим");
        Constant.stopWords.add("этими");
        Constant.stopWords.add("этих");
        Constant.stopWords.add("это");
        Constant.stopWords.add("этого");
        Constant.stopWords.add("этой");
        Constant.stopWords.add("этом");
        Constant.stopWords.add("этому");
        Constant.stopWords.add("этот");
        Constant.stopWords.add("эту");
        Constant.stopWords.add("я");
    }

}
