package by.BSUIR.documentSearch.controller;

import java.util.List;

class SimilarityMeasureController {
    private List<Double> searchQueryVector;

    SimilarityMeasureController(List<Double> searchQueryVector) {
        this.searchQueryVector = searchQueryVector;
    }

    Double similarityMeasureFor(List<Double> documentVector) {
        Double ENsearchQuary = getEN(searchQueryVector);
        Double ENdocument = getEN(documentVector);
        Double scalar = getScalarProduct(searchQueryVector, documentVector);
        double result = scalar / (ENsearchQuary * ENdocument);

        return result;
    }

    private Double getEN(List<Double> vector) {
        double amount = 0.0;

        for (Double aDouble : vector) {
            amount += (aDouble * aDouble * 1.00);
        }

        return Math.sqrt(amount);
    }

    private Double getScalarProduct(List<Double> search, List<Double> document) {
        double amount = 0.0;
        if (search.size() == 0) {
            System.out.println("Found");
            return amount;
        }
        for (int i = 0; i < search.size(); i++) {
            amount += (search.get(i) * document.get(i));
        }

        return amount;
    }
}