package by.BSUIR.documentSearch.controller;

import by.BSUIR.documentSearch.Constant;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.model.Info;

import java.util.ArrayList;
import java.util.List;

class SearchQueryController {
    private String searchRequest;
    private List<String> lemmas;
    private LemmaController lemmaController;

    SearchQueryController(String searchRequest) {
        this.searchRequest = searchRequest;
        this.lemmaController = new LemmaController();
        this.lemmas = getLemmasList();
    }

    private Boolean include(String keyWord){
        for (String lemma : lemmas) {
            if (lemma.equals(keyWord)) return true;

        }
        return false;
    }

    List<Double> getVectorOfSearchQuery(List<String> documentKeyWords) {
        List<Double> vectorOfZeroAndOneForQuerySearch = new ArrayList<>();

        for (String documentKeyWord : documentKeyWords) {
            if (include(documentKeyWord)) {
                vectorOfZeroAndOneForQuerySearch.add(1.0);
            } else {
                vectorOfZeroAndOneForQuerySearch.add(0.0);
            }
        }
        return vectorOfZeroAndOneForQuerySearch;
    }

    private List<String> getLemmasList() {
        Iterable<Info> textLemmasIterable = null;
        try {
            textLemmasIterable = lemmaController.parseText(searchRequest);
        } catch (MyStemApplicationException e) {
            e.printStackTrace();
        }

        lemmaController.processQueryInfo(textLemmasIterable);
        return lemmaController.getLemmasList();
    }

}
