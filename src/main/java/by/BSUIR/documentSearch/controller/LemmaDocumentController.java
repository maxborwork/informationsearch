package by.BSUIR.documentSearch.controller;

import by.BSUIR.documentSearch.dao.LemmaDao;
import by.BSUIR.documentSearch.dao.LemmaDocumentDao;
import by.BSUIR.documentSearch.model.Lemma;
import by.BSUIR.documentSearch.model.LemmaDocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class LemmaDocumentController {
    private LemmaDao lemmaDao;
    private LemmaDocumentDao lemmaDocumentDao;

    LemmaDocumentController(LemmaDao lemmaDao, LemmaDocumentDao lemmaDocumentDao) {
        this.lemmaDao = lemmaDao;
        this.lemmaDocumentDao = lemmaDocumentDao;
    }

    void saveLemmaDocument(int documentID, Map<String, Integer> lemmaCount) {
        List<LemmaDocument> lemmaDocuments = new ArrayList<>();
        for (Map.Entry<String, Integer> lemmaCountEntity : lemmaCount.entrySet()) {
            System.out.println(lemmaCountEntity);
            LemmaDocument lemmaDocument = new LemmaDocument(
                    lemmaDao.getLemma(lemmaCountEntity.getKey()).getId(),
                    documentID,
                    lemmaCountEntity.getValue()
            );
            lemmaDocuments.add(lemmaDocument);
        }
        lemmaDocumentDao.saveLemmaDocuments(lemmaDocuments);
    }
}
