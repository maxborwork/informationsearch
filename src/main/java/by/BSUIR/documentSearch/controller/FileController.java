package by.BSUIR.documentSearch.controller;

import by.BSUIR.documentSearch.dao.DocumentDao;
import by.BSUIR.documentSearch.dao.LemmaDao;
import by.BSUIR.documentSearch.dao.LemmaDocumentDao;
import by.BSUIR.documentSearch.model.Document;
import ru.stachek66.nlp.mystem.holding.MyStemApplicationException;
import ru.stachek66.nlp.mystem.model.Info;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class FileController {
    private List<Document> documents = new ArrayList<>();
    private DocumentDao documentDao;
    private LemmaDao lemmaDao;
    private LemmaDocumentDao lemmaDocumentDao;
    private LemmaController lemmaController;

    public FileController() {
        this.documentDao = new DocumentDao();
        this.lemmaController = new LemmaController();
        this.lemmaDao = new LemmaDao();
        this.lemmaDocumentDao = new LemmaDocumentDao();
    }

    public void parseDirectory(String path) {
        boolean isAnyAdded = false;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    boolean isAdded = processFile(file.getName(), file.getAbsolutePath());
                    if (isAdded) {
                        isAnyAdded = isAdded;
                    }
                }
            }
        }
        List<Document> dbDocumentsList = documentDao.getDocuments();

        for (Document document : dbDocumentsList) {
            int documentID = document.getId();
            DocumentController documentController = new DocumentController(document, documentDao, lemmaDao, lemmaDocumentDao);
            if (isAnyAdded) {
                documentController.calculateLemmasWeight();
            }
            document.setLemmCount(documentController.getLemmaCount(documentID));
            document.setKeyWords(documentController.getKeyWords());
            documentController.setDocument(document);
            Vector<Double> documentVector;
            if (isAnyAdded) {
                documentVector = documentController.createDocumentVector();
            } else {
                documentVector = documentController.getLemmaDocumentDao().getKeyLemmasWeight(documentID);
            }
            documents.add(
                    new Document(
                            documentID,
                            document.getTitle(),
                            document.getText(),
                            document.getPath(),
                            document.getColOfWords(),
                            document.getLemmaCount(),
                            document.getKeyWords(),
                            documentVector
                    ));
        }
    }

    private boolean processFile(String name, String path) {
        int documentID = documentDao.getDocumentId(name);
        if (documentID != 0) {
            return false;
        }

        Iterable<Info> textLemmasIterable = null;
        String fileContent = readFile(path);

        try {
            textLemmasIterable = lemmaController.parseText(fileContent);
        } catch (MyStemApplicationException e) {
            e.printStackTrace();
        }

        lemmaController.processDocumentInfo(textLemmasIterable);


        Document document = new Document(name, path, fileContent);
        document.setColOfWords(lemmaController.getColOfWords());
        documentDao.saveDocument(document);
        document.setId(documentDao.getDocumentId(name));
        document.setLemmCount(lemmaController.getLemmaCount());

        new LemmaDocumentController(lemmaDao, lemmaDocumentDao).saveLemmaDocument(document.getId(), document.getLemmaCount());
        return true;
    }


    String readFile(String path) {
        InputStream is = null;
        try {
            is = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = null;
        try {
            line = buf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        while (line != null) {
            sb.append(line).append("\n");
            try {
                line = buf.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public List<Document> getDocumentList() {
        return documents;
    }

    public LemmaController getLemmaController() {
        return lemmaController;
    }
}
