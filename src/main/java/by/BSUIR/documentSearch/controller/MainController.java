package by.BSUIR.documentSearch.controller;

import by.BSUIR.documentSearch.model.Document;
import by.BSUIR.documentSearch.model.Result;

import java.util.*;

public class MainController {
    private SearchQueryController searchQueryController;
    private List<Document> documents;
    private FileController fileController;

    public MainController(String searchRequest, FileController fileController) {
        this.fileController = fileController;
        this.documents = fileController.getDocumentList();
        this.searchQueryController = new SearchQueryController(searchRequest);
    }

    public List<Result> getListForResponse() {
        Map<Double, Document> similarityMeasureToDoc = new HashMap<>();
        List<Result> results = new ArrayList<>();

        for (Document doc : documents) {
            List<String> list = doc.getKeyWords();
            SimilarityMeasureController similarityMeasureController = new SimilarityMeasureController(
                    searchQueryController.getVectorOfSearchQuery(list)
            );
            System.out.println(doc.getTitle());
            similarityMeasureToDoc.put(similarityMeasureController.similarityMeasureFor(doc.getDocumentVector()), doc);
        }

        List<Double> docBySimilarityMeasure = new ArrayList<>(similarityMeasureToDoc.keySet());
        Collections.sort(docBySimilarityMeasure);

        if (docBySimilarityMeasure.size() > 20) {
            for (int i = 0; i < 20; i++) {
                results.add(createResult(similarityMeasureToDoc.get(docBySimilarityMeasure.get(i))));
            }
        } else {
            for (Double doubleMeasure : docBySimilarityMeasure) {
                results.add(createResult(similarityMeasureToDoc.get(doubleMeasure)));
            }
        }

        return results;
    }

    private Result createResult(Document doc) {
        List<String> list = doc.getKeyWords();
        List<Double> re = searchQueryController.getVectorOfSearchQuery(list);
        List<String> words = new ArrayList<>();
        List<String> docList = documentToList(doc.getPath());
        Map<String, String> wordsSnippets = new HashMap<>();

        for (int j = 0; j < list.size(); j++) {
            if (re.get(j) == 1) {
                words.add(list.get(j));
            }
        }

        for (String word : words) {
            String snippet = "";
            int wordIndex = docList.indexOf(word);
            if (wordIndex > 3) {
                for (int i= wordIndex-3; i < wordIndex+4; i++) {
                    snippet = snippet.concat(docList.get(i));
                }
            } else {
                for (int i= wordIndex; i < wordIndex+7; i++) {
                    snippet = snippet.concat(docList.get(i));
                }
            }
            wordsSnippets.put(word, snippet);
        }

        return new Result(doc.getPath(), words, doc.getTitle(), wordsSnippets);
    }

    private List<String> documentToList(String path) {
        String text = fileController.readFile(path);
        String[] wordsArray = text.split(" ");

        return new ArrayList<>(Arrays.asList(wordsArray));
    }
}
