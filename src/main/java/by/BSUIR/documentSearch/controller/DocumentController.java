package by.BSUIR.documentSearch.controller;

import by.BSUIR.documentSearch.dao.DocumentDao;
import by.BSUIR.documentSearch.dao.LemmaDao;
import by.BSUIR.documentSearch.dao.LemmaDocumentDao;
import by.BSUIR.documentSearch.model.Document;
import by.BSUIR.documentSearch.model.Lemma;
import by.BSUIR.documentSearch.model.LemmaDocument;
import org.apache.log4j.Logger;

import java.util.*;

public class DocumentController {

    private Logger log = Logger.getLogger(DocumentController.class);

    private DocumentDao documentDao;
    private int colOfDocumentsInBase;
    private Document document;
    private LemmaDao lemmaDao;
    private LemmaDocumentDao lemmaDocumentDao;

    DocumentController(Document document, DocumentDao documentDao, LemmaDao lemmaDao, LemmaDocumentDao lemmaDocumentDao) {
        this.document = document;
        this.documentDao = documentDao;
        this.lemmaDao = lemmaDao;
        this.lemmaDocumentDao = lemmaDocumentDao;
        this.colOfDocumentsInBase = documentDao.getDocuments().size();
    }

    Vector<Double> createDocumentVector() {
        Vector<Double> vectorForDoc = new Vector<>();
        Map<Integer, Double> numerator = new HashMap<>();
        Map<Integer, Double> keyLemmaWeights = new HashMap<>();
        double denominator;
        double amount = 0.0;
        int numberOfDocWithThisLem;
        Map<String, Integer> lemmaIdMap = lemmasListToNameIdMap();
        for (Map.Entry<String, Integer> lemmaCountEntity : document.getLemmCount().entrySet()) {
            if (document.getKeyWords().contains(lemmaCountEntity.getKey())) {
                int lemmaID = lemmaIdMap.get(lemmaCountEntity.getKey());
                numberOfDocWithThisLem = lemmaDocumentDao.getColOfDocumentsForLemma(lemmaID);
                numerator.put(lemmaID,
                        lemmaCountEntity.getValue() * Math.log(colOfDocumentsInBase * 1.0 / numberOfDocWithThisLem));
            }
        }

        for (Map.Entry<Integer, Double> aDouble : numerator.entrySet()) {
            amount += aDouble.getValue() * aDouble.getValue();
        }

        denominator = Math.sqrt(amount);

        for (Map.Entry<Integer, Double> aDouble : numerator.entrySet()) {
            double keyLemmaWeight = aDouble.getValue() * 1.0 / denominator;
            keyLemmaWeights.put(aDouble.getKey(), keyLemmaWeight);
            vectorForDoc.add(keyLemmaWeight);
        }

        lemmaDocumentDao.setKeyLemmaWeights(document.getId(), keyLemmaWeights);

        return vectorForDoc;
    }

    List<String> getKeyWords() {
        return lemmaDocumentDao.getKeyLemmasFromDocument(document.getId());
    }

    void calculateLemmasWeight() {
        List<LemmaDocument> lemmaDocuments = lemmaDocumentDao.getLemmaCountFromDocument(document.getId());
        for (LemmaDocument lemmaDocument : lemmaDocuments) {
            int lemmaID = lemmaDocument.getLemmaID();
            double weight = getLemmaWeight(document.getId(), lemmaID);

            lemmaDocument.setLemmaWeight(weight);
        }
        lemmaDocumentDao.setLemmaWeights(lemmaDocuments);
        log.info("Weights calculated");
    }

    private double getLemmaWeight(int documentID, int lemmaID) {
        double idf = getLemmaInverseFrequency(lemmaID);
        double tf = calcLemmaFrequency(lemmaID, documentID);
        return tf * idf;
    }


    private double getLemmaInverseFrequency(int lemmaID) {
        int numberOfDocWithLemma = lemmaDocumentDao.getColOfDocumentsForLemma(lemmaID);
        int colOfDocumentsInBase = documentDao.getDocuments().size();
        return Math.log(colOfDocumentsInBase * 1.0 / numberOfDocWithLemma);
    }

    private double calcLemmaFrequency(int lemmaID, int documentID) {
        int lemmaCount = lemmaDocumentDao.getLemmaCountFromDocument(lemmaID, documentID);
        int wordsInDocument = documentDao.getDocumentColOfWords(documentID);
        return lemmaCount * 1.0 / wordsInDocument;
    }

    private Map<Integer, String> lemmasListToIdNameMap() {
        Map<Integer, String> idLemma = new HashMap<>();
        List<Lemma> lemmaList = lemmaDao.getLemmas();
        for (Lemma lemma : lemmaList) {
            idLemma.put(lemma.getId(), lemma.getName());
        }
        return idLemma;
    }

    private Map<String, Integer> lemmasListToNameIdMap() {
        Map<String, Integer> idLemma = new HashMap<>();
        List<Lemma> lemmaList = lemmaDao.getLemmas();
        for (Lemma lemma : lemmaList) {
            idLemma.put(lemma.getName(), lemma.getId());
        }
        return idLemma;
    }

    Map<String, Integer> getLemmaCount(int documentID) {
        Map<String, Integer> lemmaCount = new HashMap<>();
        Map<Integer, String> idLemma = lemmasListToIdNameMap();
        List<LemmaDocument> lemmaDocuments = lemmaDocumentDao.getLemmaCountFromDocument(documentID);
        for (LemmaDocument lemmaDocument : lemmaDocuments) {
            lemmaCount.put(idLemma.get(lemmaDocument.getLemmaID()), lemmaDocument.getNumOfRepeats());
        }
        return lemmaCount;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    LemmaDocumentDao getLemmaDocumentDao() {
        return lemmaDocumentDao;
    }
}
