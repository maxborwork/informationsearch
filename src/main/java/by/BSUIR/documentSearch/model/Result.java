package by.BSUIR.documentSearch.model;

import java.util.List;
import java.util.Map;

public class Result {
    String path;
    List<String> words;
    String name;
    Map<String, String> wordsSnippets;

    public Result(String path, List<String> words, String title, Map<String, String> wordsSnippets) {
        this.path = path;
        this.words = words;
        this.name = title;
        this.wordsSnippets = wordsSnippets;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public List<String> getWords() {
        return words;
    }

    public Map<String, String> getWordsSnippets() {
        return wordsSnippets;
    }
}
